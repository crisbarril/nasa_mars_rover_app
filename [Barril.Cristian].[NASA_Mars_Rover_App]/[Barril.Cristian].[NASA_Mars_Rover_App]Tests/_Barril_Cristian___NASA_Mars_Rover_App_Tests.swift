//
//  _Barril_Cristian___NASA_Mars_Rover_App_Tests.swift
//  [Barril.Cristian].[NASA_Mars_Rover_App]Tests
//
//  Created by Cristian Barril on 2/12/17.
//  Copyright © 2017 Cristian Barril. All rights reserved.
//

import XCTest
@testable import _Barril_Cristian___NASA_Mars_Rover_App_

/*
 
        Test functions separately to prevent "OVER RATE LIMIT" error
 
 */

class _Barril_Cristian___NASA_Mars_Rover_App_Tests: XCTestCase {
    
    var expectation:XCTestExpectation?
    var apiManager:ApiManager?
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        apiManager = ApiManager.sharedInstance
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        apiManager = nil
        super.tearDown()
    }
    
    func testApiResponseType() {
        let expectation = self.expectation(description: ("GET \(kNasaMarsRoverUrl)"))
        let session = URLSession.shared
        let URL = NSURL(string: kNasaMarsRoverUrl)!
        let task = session.dataTask(with: URL as URL) { data, response, error in
            XCTAssertNotNil(data, "No data")
            XCTAssertNil(error, "Response error")
            
            if let HTTPResponse = response as? HTTPURLResponse,
                let responseURL = HTTPResponse.url,
                let MIMEType = HTTPResponse.mimeType
            {
                XCTAssertEqual(responseURL.absoluteString, URL.absoluteString, "HTTP URL doesn´t match")
                XCTAssertEqual(MIMEType, "application/json", "HTTP response is not application/json")
            }
            
            expectation.fulfill()
        }
        
        task.resume()
        
        waitForExpectations(timeout: task.originalRequest!.timeoutInterval) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
            task.cancel()
        }
    }
    
    func testApiData () {
        let expectation = self.expectation(description: ("GET \(kNasaMarsRoverUrl)"))
        let session = URLSession.shared
        let URL = NSURL(string: kNasaMarsRoverUrl)!
        let task = session.dataTask(with: URL as URL) { data, response, error in
            do{
                let JSON = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                XCTAssertNotNil(JSON["photos"] as! [AnyObject], "Data is not an Array")
            }
            catch {
                XCTAssertNil(error, "Did't receive JSON")
            }
            
            expectation.fulfill()
        }
        
        task.resume()
        
        waitForExpectations(timeout: task.originalRequest!.timeoutInterval) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
            task.cancel()
        }
    }
}
