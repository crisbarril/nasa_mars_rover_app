//
//  ApiManager.swift
//  [Barril.Cristian].[NASA_Mars_Rover_App]
//
//  Created by Cristian Barril on 2/12/17.
//  Copyright © 2017 Cristian Barril. All rights reserved.
//

import Foundation
import Alamofire

let kNasaMarsRoverUrl = "https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?sol=1000&api_key=DEMO_KEY"
typealias ApiResponseBlock = (_ serverResponse: [PhotoInfo]) -> (Void)

class ApiManager {
    
    static let sharedInstance = ApiManager()
    
    func downloadData(completion: @escaping ApiResponseBlock) {
        
        Alamofire.request(kNasaMarsRoverUrl).validate().responseJSON { response in
            switch response.result {
            case .success:
                if let actualData = response.data {
                    do {
                        let parsedResult = try JSONSerialization.jsonObject(with: actualData, options: []) as! [String:AnyObject]
                        
                        if let data = parsedResult["photos"] as? [AnyObject] {
                            let nasaData = self.parseJsonObject(json: data)
                            
                            completion(nasaData)
                        } else {
                            print("Could't parse result data")
                            completion([])
                        }
                    } catch let error as NSError {
                        print(error)
                        completion([])
                    }
                }
            case .failure:
                print("Request failure")
                completion([])
            }
        }
    }
    
    private func parseJsonObject(json: [AnyObject]) -> [PhotoInfo] {
        var result = [PhotoInfo]()
        
        for item in json {
            if let itemDic = item as? [String:Any] {
                guard let earth_date = itemDic["earth_date"] as? String else {
                    print("Earth_date missing")
                    continue
                }
                
                guard let imageUrl = itemDic["img_src"] as? String else {
                    print("Image Url missing")
                    continue
                }
                
                guard let camera = itemDic["camera"] as? [String:AnyObject] else {
                    print("Camera missing")
                    continue
                }
                
                guard let full_name = camera["full_name"] as? String else {
                    print("Camera full name missing")
                    continue
                }
                
                let photoInfo = PhotoInfo.init(earth_date: earth_date, cameraFullName: full_name, imageUrl: imageUrl)
                result.append(photoInfo)
            }
        }
        
        return result
    }
}
