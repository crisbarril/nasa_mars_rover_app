//
//  PhotoInfo.swift
//  [Barril.Cristian].[NASA_Mars_Rover_App]
//
//  Created by Cristian Barril on 2/12/17.
//  Copyright © 2017 Cristian Barril. All rights reserved.
//

import Foundation

struct PhotoInfo {
    var earth_date: String
    var cameraFullName: String
    var imageUrl: String
}
