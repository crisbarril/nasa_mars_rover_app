//
//  DetailViewController.swift
//  [Barril.Cristian].[NASA_Mars_Rover_App]
//
//  Created by Cristian Barril on 2/12/17.
//  Copyright © 2017 Cristian Barril. All rights reserved.
//

import UIKit
import AlamofireImage

class DetailViewController: UIViewController {
    
    @IBOutlet weak var photoImageView: UIImageView!
    var photoInfo: PhotoInfo!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setup(withPhotoInfo photoInfo: PhotoInfo) {
        self.photoInfo = photoInfo
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let url = URL(string: photoInfo.imageUrl)!
        let placeholderImage = UIImage(named: "ImagePlaceholder")!
        
        photoImageView.af_setImage(
            withURL: url,
            placeholderImage: placeholderImage,
            imageTransition: .flipFromRight(0.3)
        )
    }
}
