//
//  ViewController.swift
//  [Barril.Cristian].[NASA_Mars_Rover_App]
//
//  Created by Cristian Barril on 2/12/17.
//  Copyright © 2017 Cristian Barril. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var apiDataArray = Array<PhotoInfo>()
    let manager = NetworkReachabilityManager(host: "www.apple.com")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        title = "NASA MARS ROVER"
        
        //Default value
        self.tableView.isHidden = true
        
        manager?.listener = { status in
            print("Network Status Changed: \(status)")
            switch status {
            case .notReachable:
                self.tableView.isHidden = true
            default:
                self.tableView.isHidden = false
                self.loadData()
            }
        }
        
        manager?.startListening()
    }
    
    func loadData() {
        let loadingView = UIView.init(frame: self.view.frame)
        loadingView.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.7)
        view.addSubview(loadingView)
        
        let activityIndicator = UIActivityIndicatorView(frame: CGRect.init(x: 0, y: 0, width: 50, height: 50))
        activityIndicator.center = loadingView.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        activityIndicator.color = UIColor.red
        
        loadingView.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        let successBlock: ApiResponseBlock = {(data) -> (Void) in
            self.apiDataArray = data
            self.tableView.reloadData()
            loadingView.removeFromSuperview()
        }
        
        let noResultBlock = {
            let alert = UIAlertController(title: "Search result", message: "No data found. Try again later.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler:nil))
            
            self.present(alert, animated: true, completion: {
                loadingView.removeFromSuperview()
            })
        }
        
        ApiManager.sharedInstance.downloadData { (data) -> (Void) in
            if (data.count == 0) {
                noResultBlock()
            }
            else {
                successBlock(data)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

// MARK: - UITableViewDataSource
extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return apiDataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Table view cells are reused and should be dequeued using a cell identifier.
        let kCellIdentifier = "kCustomTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier, for: indexPath) as! CustomTableViewCell
        
        let photoInfo = apiDataArray[indexPath.row]
        
        //Different background color for cells with an odd index
        if indexPath.row % 2 == 0 {
            cell.backgroundColor = UIColor.white
        }
        else {
            cell.backgroundColor = UIColor.lightGray
        }
        
        cell.earthDateLabel.text = photoInfo.earth_date
        cell.cameraFullNameLabel.text = photoInfo.cameraFullName
        
        let url = URL(string: photoInfo.imageUrl)!
        let placeholderImage = UIImage(named: "ImagePlaceholder")!
        
        cell.photoImageView.af_setImage(
            withURL: url,
            placeholderImage: placeholderImage,
            imageTransition: .flipFromRight(0.3)
        )
        
        return cell
    }
}

// MARK: - UITableViewDelegate
extension ViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let photoInfo = apiDataArray[indexPath.row]
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let detailViewController = storyBoard.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        detailViewController.setup(withPhotoInfo: photoInfo)
        
        navigationController?.pushViewController(detailViewController, animated: true)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - CustomTableViewCell

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var earthDateLabel: UILabel!
    @IBOutlet weak var cameraFullNameLabel: UILabel!
    @IBOutlet weak var photoImageView: UIImageView!
    
    override func prepareForReuse() {
        photoImageView.image = nil
    }
}
